import random as rand
import copy
import time
import moviecluster as mvc
import cluster
import utils
# from IPython import embed


# Doesn't work
def our_algorithm5(movie_list, movies_objects, p_singles, p_doubles, **kwargs):
    Cluster = []
    movies = copy.deepcopy(movies_objects)
    for m in movies:
        Cluster.append(cluster.Cluster(m))

    while movies:
        pivot = rand.choice(movies)
        clust = cluster.Cluster(pivot)
        movies.remove(pivot)
        movie = get_best_candidate(pivot, movies, p_singles=p_singles, p_doubles=p_doubles)
        if movie == False:
            Cluster.append(clust)
            continue
        else:
            clust.add_movie(movie)
            movies.remove(movie)

        Cluster.append(clust)

    return Cluster


def get_best_candidate(pivot, sample, **kwargs):
    p_singles = kwargs['p_singles']
    p_doubles = kwargs['p_doubles']
    deltas = {}
    for movie in sample:
        movie_pair = [pivot.id, movie.id]
        movie_pair.sort()
        delta = p_doubles[tuple(movie_pair)] - (p_singles[pivot.id] * p_singles[movie.id])
        if delta > 0:
            deltas[delta] = movie

    # print deltas

    return False if (deltas.keys() == []) or max(deltas.keys()) <= 0 else deltas[max(deltas.keys())]


def our_algorithm6(movie_list, movies_objects, p_singles, p_doubles, **kwargs):

    remaining_movies = copy.deepcopy(movies_objects)
    Cluster = []
    while remaining_movies:

        pivot = rand.choice(remaining_movies)
        clust = cluster.Cluster(pivot)
        remaining_movies.remove(pivot)
        # candidates = rand.sample(remaining_movies, min(10, len(remaining_movies)))
        while True:
            movie = get_best_candidate_for_our_algorithm6(clust, remaining_movies, p_singles=p_singles, p_doubles=p_doubles)
            # print movie
            if movie == False:
                Cluster.append(clust)
                break
            else:
                clust.add_movie(movie)
                remaining_movies.remove(movie)

    return Cluster


def get_best_candidate_for_our_algorithm6(clust, sample, **kwargs):
    p_singles = kwargs['p_singles']
    p_doubles = kwargs['p_doubles']
    deltas = []

    for candidate in sample:
        for movie in clust.movies:
            movie_pair = [movie.id, candidate.id]
            movie_pair.sort()
            delta = p_doubles[tuple(movie_pair)] - (p_singles[movie.id] * p_singles[candidate.id])
            deltas.append(delta)

        if all(i > 0 for i in deltas):
            return candidate
        else:
            deltas = []

    return False


# def our_algorithm71(movie_list):
#     starttime = time.time()
#     vectors, users, stats = mvc.calc_vectors(movie_list)
#     p_singles, p_doubles = mvc.calc_p_singles_and_doubles(vectors, users, movie_list, stats)
#     movies = copy.deepcopy(movie_list)
#
#     Cluster = [[m] for m in movies]
#     success = True
#     while success:
#         success = False
#         # from random import shuffle
#         # shuffle(Cluster)
#         # del shuffle
#         best_reduction = 0
#         bucket_indices = ()
#         for c1 in Cluster:
#             c1_cost = mvc.calc_cluster(c1, p_singles, p_doubles)
#             for c2 in Cluster:
#                 if c1 == c2:
#                     continue
#                 c2_cost = mvc.calc_cluster(c2, p_singles, p_doubles)
#                 possible_cost = mvc.calc_cluster(c1 + c2, p_singles, p_doubles)
#                 if possible_cost < c1_cost + c2_cost:
#                     current_reduction = c1_cost + c2_cost - possible_cost
#                     if current_reduction > best_reduction:
#                         bucket_indices = (Cluster.index(c1), Cluster.index(c2))
#                         best_reduction = current_reduction
#
#         if bucket_indices != ():
#             try:
#                 sorted(bucket_indices)
#                 c2 = Cluster.pop(bucket_indices[1])
#                 c1 = Cluster.pop(bucket_indices[0])
#
#             except IndexError:
#                 print bucket_indices
#                 print
#                 print c1, c2
#                 exit(1)
#             Cluster.append(c1 + c2)
#
#             success = True
#
#
#     print "Time took algorithm2: ", time.time() - starttime
#     print Cluster
#     cost = mvc.calc_cluster_cost(Cluster, p_singles, p_doubles)
#     print "Cost: ", cost
#     return cost, Cluster


def our_algorithm7(movie_list, movies_objects, p_singles, p_doubles, **kwargs):

    movies = copy.deepcopy(movies_objects)

    Cluster = []
    for m in movies:
        Cluster.append(cluster.Cluster(m))

    success = True
    while success:
        success = False
        best_reduction = 0
        bucket_indices = ()
        for c1 in Cluster:
            c1_cost = utils.calc_cluster(c1.movies, p_singles, p_doubles)
            for c2 in Cluster:
                if c1 == c2:
                    continue
                c2_cost = utils.calc_cluster(c2.movies, p_singles, p_doubles)
                possible_cost = utils.calc_cluster(c1.movies + c2.movies, p_singles, p_doubles)
                if possible_cost < c1_cost + c2_cost:
                    current_reduction = c1_cost + c2_cost - possible_cost
                    if current_reduction > best_reduction:
                        bucket_indices = (Cluster.index(c1), Cluster.index(c2))
                        best_reduction = current_reduction

        if bucket_indices != ():
            try:
                sorted(bucket_indices)
                c2 = Cluster.pop(bucket_indices[1])
                c1 = Cluster.pop(bucket_indices[0])

            except IndexError:
                print bucket_indices
                print
                print c1, c2
                exit(1)
            Cluster.append(c1.merge(c2))

            success = True

    return Cluster


def same_genre_clusters(movie_list, movies_objects, p_singles, p_doubles, **kwargs):

    movies = copy.deepcopy(movies_objects)

    Cluster = []
    for m in movies:
        Cluster.append(cluster.Cluster(m))

    success = True
    while success:
        success = False
        best_reduction = 0
        bucket_indices = ()
        for c1 in Cluster:
            c1_cost = utils.calc_cluster(c1.movies, p_singles, p_doubles)
            for c2 in Cluster:
                if c1 == c2:
                    continue
                c2_cost = utils.calc_cluster(c2.movies, p_singles, p_doubles)
                possible_cost = utils.calc_cluster(c1.movies + c2.movies, p_singles, p_doubles)
                if possible_cost < c1_cost + c2_cost:
                    current_reduction = c1_cost + c2_cost - possible_cost
                    if current_reduction > best_reduction and c1.genre_fits_to_cluster(c2):
                        bucket_indices = (Cluster.index(c1), Cluster.index(c2))
                        best_reduction = current_reduction

        if bucket_indices != ():
            try:
                sorted(bucket_indices)
                c2 = Cluster.pop(bucket_indices[1])
                c1 = Cluster.pop(bucket_indices[0])

            except IndexError:
                print bucket_indices
                print
                print c1, c2
                exit(1)
            Cluster.append(c1.merge(c2))

            success = True

    return Cluster


def our_algorithm2(movie_list, movies, p_singles, p_doubles, **kwargs):
    remaining_movies = copy.deepcopy(movies)
    Cluster = []
    for m in movies:
        Cluster.append(cluster.Cluster(m))

    success = True
    while success:
        success = False
        from random import shuffle
        shuffle(Cluster)
        del shuffle
        for c in Cluster:
            current_cost = utils.calc_cluster(c.movies, p_singles, p_doubles)
            best_reduction = 0
            bucket_index = -1
            for c2 in Cluster:
                if c == c2:
                    continue
                c2_cost = utils.calc_cluster(c2.movies, p_singles, p_doubles)
                possible_cost = utils.calc_cluster(c.movies + c2.movies, p_singles, p_doubles)
                if possible_cost < current_cost + c2_cost:
                    current_reduction = current_cost + c2_cost - possible_cost
                    if current_reduction > best_reduction:
                        bucket_index = Cluster.index(c2)
                        best_reduction = current_reduction

            if bucket_index != -1:

                new_clust_movies = c.movies + Cluster.pop(bucket_index).movies
                clust = cluster.Cluster(utils.create_dummy_movie())
                clust.from_movies(new_clust_movies)
                Cluster.remove(c)
                Cluster.append(clust)
                success = True
                break


    return Cluster


def our_algorithm(movie_list, movies, p_singles, p_doubles, **kwargs):
    Cluster = []

    # implementation goes here:

    remaining_movies = copy.deepcopy(movies)
    while remaining_movies:
        pivot = rand.choice(remaining_movies)
        # clust = [pivot]
        clust = cluster.Cluster(pivot)
        remaining_movies.remove(pivot)
        for movie in remaining_movies:
            try:
                if condition1(clust, movie, p_singles, p_doubles):
                    clust.add_movie(movie)
                    remaining_movies.remove(movie)
            except KeyError:
                t = tuple(sorted((pivot, movie)))
                print "is (%s,%s) in p_doubles?" % t, t in p_doubles
                exit()
        Cluster.append(clust)


    return Cluster


def condition(pivot, movie, p_singles, p_doubles):
    vals = p_doubles.values()
    average_value = float(sum(vals) / len(vals))
    return p_doubles[tuple(sorted((pivot.id, movie.id)))] >= average_value #1.326804922367742e-07


def condition1(clust, movie, p_singles, p_doubles):
    count = 0
    for m in clust.movies:
        if p_singles[m.id] * p_singles[movie.id] <= p_doubles[tuple(sorted((m.id, movie.id)))]:
            count += 1
    return count >= len(clust)
