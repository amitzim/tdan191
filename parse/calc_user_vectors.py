from IPython import embed
# from parse import db
# import sys
import time
import itertools
import db
num_users = N = 6040
num_movies = k = 3883
# global vectors
# global movies


def main():
    start = time.time()
    # global vectors
    # global movies
    users = db.fetch_users_id()
    movies = db.fetch_movies_id()
    ratings = db.fetch_ratings()
    stats = dict(db.get_all_users_statistics())
    # embed()
    vectors = dict()
    for user in users:
        vectors[user] = dict()
        for movie in movies:
            vectors[user][movie] = 0

    for record in ratings:
        try:
            vectors[record[0]][record[1]] = 1
        except KeyError:
            print record
            embed()
            exit()

    print "Finished calculate vectors..."
    print "time is: ", (time.time() - start)
    # calc_p_single_to_all_movies()
    # print "time is: ", (time.time() - start) / 60.
    calc_p_couple_to_all(vectors, movies, stats)
    print "time is: ", (time.time() - start)


def calc_p_single_to_all_movies():
    # start = timestamp()
    global movies
    res = list()
    for m in movies:
        res.append((m, calculate_p_single(m)))

    # end = timestamp()
    # total_time = end - start
    # print "Total execution time: ", total_time
    # print res
    # print "Total execution time: ", total_time
    # print "this is the res", res
    print
    print "finished calculate all singles"
    # exit()
    db.add_p_single(res)
    print
    print "finished load to db all singles"


def calc_p_couple_to_all(vectors, movies, stats):
    # global movies
    # movies = movies[:10]
    index = 0
    # for m_j in movies:
    #     for m_t in movies[m_j:]:
    #         index += 1
    #         res.append((m_j, m_t, calculate_p_couple(m_j, m_t, vectors)))
    #         if index % 10000 == 0:
    #             print "now at ", index, "/", 7536903
    #             print "Time passed:", time.time() - from_start

    from_start = time.time()
    subsets = set(itertools.combinations(movies, 2))
    # res = list()
    print "Time took to calculate subsets:", time.time() - from_start
    # for rec in subsets:
    #     index += 1
    # res.append((rec[0], rec[1], calculate_p_couple(rec[0], rec[1], vectors, stats)))
    res = [(rec[0], rec[1], calculate_p_couple(rec[0], rec[1], vectors, stats)) for rec in subsets]
    # if index % 10000 == 0:
    #     print "now at ", index, "/", 7536903
    #     print "Time passed:", time.time() - from_start

    print "this is the res", res
    print
    print "finished calculate all doubles"
    # db.add_p_couples(res)
    # print
    # print "finished load to db all singles"


def calculate_p_couple(m1, m2, vectors, stats):
    # global k
    # global N
    # global vectors
    # start = time.time()
    N = 6040
    k = 3883
    one_over_n_plus_one = 0.00016553550736633007  # 1. / (N + 1)
    two_over_k = 1.32680492239319e-07  # 2. / (k * (k - 1))
    p_movie = 0.
    # print "calculate_p_couple: after declaring vars: ", time.time() - start
    for user_id, movie_vector in vectors.iteritems():
        v_j = movie_vector.get(m1)
        v_t = movie_vector.get(m2)
        if not (v_j and v_t):
            continue
        # print "user_id: ", user_id
        n_i = stats[user_id]
        # print "n_i:", n_i
        # exit()
        # embed()
        # n_i = sum(movie_vector.values())

        p_movie += (2. / (n_i * (n_i - 1)))

    return one_over_n_plus_one * (two_over_k + p_movie)


def calculate_p_single(movie):
    global k
    global N
    global vectors

    one_over_n_plus_one = 1. / (N + 1)
    two_over_k = 2. / k
    p_movie = 0.
    # start = time.time()
    # index = 0
    # last_iter_time = time.time()
    for movie_vector in vectors.itervalues():
        v_i = movie_vector.get(movie)
        if not v_i:
            continue
        # p_movie += (2. / calc_n_i(user)) * movie_vector.get(movie) # way slower than simply counting
        # p_movie += (2. / reduce(lambda x, y: x + y, movie_vector.values(), 0)) * movie_vector.get(movie) # very slow!
        # index += 1
        # iterator_time = time.time() - last_iter_time
        # print index, "Time to calc next iteration: ", iterator_time
        # start = time.time()
        n_i = sum(movie_vector.values())
        # end = time.time()
        # print index, " Total time in ms: ", end - start
        # start = time.time()
        # end = time.time()
        # print index, " Total time in ms: ", end - start
        # start = time.time()
        p_movie += (2. / n_i)
        # end = time.time()
        # print index, " Total time in ms: ", end - start
        # last_iter_time = time.time()
        # embed()
        # exit()

    # end = time.time()
    # print "Total time in ms: ", end - start

    return one_over_n_plus_one * (two_over_k + p_movie)


# def calc_n_i(user_id):
#     return db.get_user_movie_watched(user_id)


if __name__ == "__main__":
    main()
