import sqlite3
import os

database_file_name = './fake_database.db'
database_file_name = './database1.db'


def create_db():
    try:
        os.remove(database_file_name)
    except OSError:
        pass
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("""CREATE TABLE Users (
                        user_id INTEGER PRIMARY KEY NOT NULL,
                        gender TEXT NOT NULL,
                        age INTEGER NOT NULL,
                        occupation INTEGER NOT NULL,
                        zipcode INTEGER NOT NULL)""")
        cursor.execute("""CREATE TABLE Movies (
                        movie_id INTEGER PRIMARY KEY NOT NULL,
                        title TEXT NOT NULL,
                        genre TEXT NOT NULL)""")
        cursor.execute("""CREATE TABLE Ratings (
                        rating_id INTEGER PRIMARY KEY NOT NULL,
                        user_id INTEGER NOT NULL,
                        movie_id INTEGER NOT NULL,
                        rating INTEGER NOT NULL,
                        timestamp INTEGER NOT NULL,
                        FOREIGN KEY (user_id) REFERENCES Users(user_id),
                        FOREIGN KEY (movie_id) REFERENCES Movies(movie_id))""")
    dbcon.commit()
    dbcon.close()


def load_users():
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        with open('../data/users.dat', 'r') as f:
            # content = f.readlines()
            for line in f:
                line = line.strip().split('::')
                cursor.execute("""INSERT INTO Users
                                    (user_id, gender, age, occupation, zipcode)
                                    VALUES (?, ?, ?, ?, ?)""", tuple(line))
                dbcon.commit()
    dbcon.close()


def load_movies():
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        dbcon.text_factory = str
        with open('../data/movies.dat', 'r') as f:
            # content = f.readlines()
            for line in f:
                line = line.strip().split('::')
                cursor.execute("""INSERT INTO Movies
                                    (movie_id, title, genre)
                                    VALUES (?, ?, ?)""", tuple(line))
                dbcon.commit()
    dbcon.close()


def load_ratings():
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        dbcon.text_factory = str
        with open('../data/ratings.dat', 'r') as f:
            # content = f.readlines()
            for line in f:
                line = line.strip().split('::')
                cursor.execute("""INSERT INTO Ratings
                                    (user_id, movie_id, rating, timestamp)
                                    VALUES (?, ?, ?, ?)""", tuple(line))
                dbcon.commit()
    dbcon.close()


def fetch_ratings():
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("SELECT user_id, movie_id FROM Ratings")
        return cursor.fetchall()


def fetch_users_id():
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("SELECT user_id FROM Users")
        data = cursor.fetchall()
        users_list = list()
        # for rec in data:
        #     users_list.append(rec[0])

        # return users_list
        return [rec[0] for rec in data]


def fetch_movies_id():
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("SELECT movie_id FROM Movies")
        data = cursor.fetchall()
        movies_list = list()
        # for rec in data:
        #     movies_list.append(rec[0])

        # return movies_list
        return [rec[0] for rec in data]


def add_user_statistics():
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("ALTER TABLE Users ADD COLUMN movies_watched INTEGER")
        cursor.execute("""UPDATE Users
                                    SET movies_watched = (
                                        SELECT COUNT (user_id)
                                        FROM Ratings
                                        WHERE Ratings.user_id = Users.user_id
                                        GROUP BY user_id
                                        )""")
        dbcon.commit()


def get_all_users_statistics():
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("""SELECT user_id, movies_watched FROM Users""")

        return cursor.fetchall()


def get_user_movie_watched(user_id):
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("""SELECT movies_watched FROM Users WHERE Users.user_id == ?""", (user_id,))

        return cursor.fetchone()[0]


def add_p_single(movie_to_prob):
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("""ALTER TABLE Movies ADD COLUMN p_single REAL""")
        for rec in movie_to_prob:
            try:
                cursor.execute("""UPDATE Movies SET p_single = (?) WHERE Movies.movie_id == (?)""", (rec[1], rec[0]))
            except sqlite3.IntegrityError:
                print "printing error rec", rec
                exit()
        dbcon.commit()


def add_p_couples(lst):
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("""CREATE TABLE Probs (
                            j INTEGER NOT NULL,
                            t INTEGER NOT NULL,
                            value REAL NOT NULL,
                            plus_or_minus TEXT)""")
        for rec in lst:
            cursor.execute("""INSERT INTO Probs (j, t, value) VALUES (?, ?, ?)""", rec)
        dbcon.commit()


if __name__ == '__main__':
    pass
    # create_db()
    # load_users()
    # load_movies()
    # load_ratings()
