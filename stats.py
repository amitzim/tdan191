from subprocess import call
from os import remove
import sys
# from IPython import embed

number_of_tasks = 20
filename = 'results.txt'
alg = sys.argv[1]

# with sqlite3.connect('../database_with_p.db') as dbcon:
#     curosr = dbcon.cursor()
res = 0.
args = ['python', 'moviecluster.py', '../database_with_p.db', alg, 'subset_movies.txt']
for it in range(1, number_of_tasks + 1):
    call(args)
    print "ITERATION NUMBER: %d" % it


with open('results.txt', 'r') as f:
    data = f.readlines()
    # embed()
    for d in data:
        res += float(d.strip())


remove(filename)
print data
print "Average cost is: ", res / number_of_tasks

