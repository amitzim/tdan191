class Movie:

    def __init__(self, record):
        self.id, self.title, self.genre = record
        self.genre = self.genre.split('|')
        # self.p_single = p_single

    def __str__(self):
        return "%d %s %s" % (self.id, self.title, self.genre)

