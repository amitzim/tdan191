import copy
import os
import random as rand
import sqlite3
import sys

import algorithms as algo
import cluster
from hill_climbing import hill_climbing
from triples_algorithm import our_algorithm_triples
from utils import *


def check_ratings(movie_list):
    with sqlite3.connect('./database.db') as dbcon:
        cursor = dbcon.cursor()
        data = cursor.execute("select movie_id, count(movie_id) from Ratings where movie_id in %s group by movie_id" % tuple(movie_list).__str__()).fetchall()
        data = dict(map(lambda x: (x[0], x[1]), data))
        movies = [k for k, v in data.iteritems() if v >= 10]
        delta = list(set(movie_list) - set(movies))
        for bad_movie in delta:
            try:
                num_ratings = data[bad_movie]
            except KeyError:
                num_ratings = 0
            sys.stderr.write("Movie %d ignored because it has only %d ratings\n" % (bad_movie, num_ratings))

    return movies


def main():
    data_set_path, algorithm, movie_subset_file_path = parse_args()
    # print data_set_path, algorithm.__name__, movie_subset_file_path
    movie_list = sorted(check_ratings(parse_subset_file(movie_subset_file_path)))
    movies = create_movies(movie_list)
    
    vectors, users, stats = calc_vectors(movie_list)
    p_singles, p_doubles = calc_p_singles_and_doubles(vectors, users, movie_list, stats)

    Cluster = algorithm(movie_list, movies, p_singles, p_doubles, vectors=vectors, stats=stats)

    try:
        size = reduce(lambda x, y: x + len(y), Cluster, 0)
        assert len(movie_list) == size
    except AssertionError as e:
        print "Cluster size is:", size
        print e
        exit(1)
    cost = calc_cluster_cost(Cluster, p_singles, p_doubles)
    print cluster_to_string(Cluster), cost
    # print cost


def parse_subset_file(movie_subset_file_path):
    movie_list = list()
    with open(movie_subset_file_path, 'r') as f:
        for line in f:
            try:
                movie_list.append(int(line.strip()))
            except ValueError:
                pass

    return movie_list


def pivot_algorithm(movie_list, movies, p_singles, p_doubles, **kwargs):
    Cluster = []
    remaining_movies = copy.deepcopy(movies)
    while remaining_movies:
        pivot = rand.choice(remaining_movies)
        # clust = [pivot]
        clust = cluster.Cluster(pivot)
        remaining_movies.remove(pivot)
        for movie in remaining_movies:
            try:
                movie_pair = [pivot.id, movie.id]
                movie_pair.sort()
                if p_singles[pivot.id] * p_singles[movie.id] <= p_doubles[tuple(movie_pair)]:
                    # clust.append(movie)
                    clust.add_movie(movie)
                    remaining_movies.remove(movie)
            except KeyError:
                t = set_tuple(pivot.id, movie.id)
                print "is %s in p_doubles?" % t.__str__(), t in p_doubles
                exit()
        Cluster.append(clust)

    return Cluster


def parse_args():
    try:
        data_set_path = sys.argv[1]
        chosen_alg = int(sys.argv[2])
        movie_subset_file_path = sys.argv[3]
    except IndexError:
        sys.stderr.write("insufficient number of arguments\n")
        exit(1)
    except ValueError as e:
        sys.stderr.write(str(e) + '\n')
        sys.stderr.write("Algorithm number has to be 1 or 2, not " + str(sys.argv[2]) + '\n')
        exit()

    algs = {1: pivot_algorithm, 2: algo.our_algorithm7
            }
            # ,3: algo.our_algorithm2,
            # 5: our_algorithm_triples,
            # 6: hill_climbing,
            # 8: algo.our_algorithm6, 9: algo.our_algorithm, 10: algo.same_genre_clusters}

    try:
        algorithm = algs[chosen_alg]
    except KeyError as e:
        sys.stderr.write(str(e) + '\n')
        sys.stderr.write("Algorithm number has to be 1 or 2, not " + str(sys.argv[2]) + '\n')
        exit()

    if not os.path.exists(movie_subset_file_path):
        sys.stderr.write(movie_subset_file_path + " Not Exist...\nplease specify a valid path to file\n")
        exit()

    return data_set_path, algorithm, movie_subset_file_path


def calculate_p_single(movie, k, N, vectors):
    one_over_n_plus_one = 1. / (N + 1)
    two_over_k = 2. / k
    p_movie = 0.
    for movie_vector in vectors.itervalues():
        v_i = movie_vector.get(movie)
        if not v_i:
            continue

        n_i = sum(movie_vector.values())
        p_movie += (2. / n_i)

    return one_over_n_plus_one * (two_over_k + p_movie)


def calc_vectors(movies):
    users = db.fetch_users_id()
    ratings = db.fetch_ratings()
    stats = dict(db.get_all_users_statistics())
    vectors = dict()
    for user in users:
        vectors[user] = dict()
        for movie in movies:
            vectors[user][movie] = 0

    for record in ratings:
        try:
            vectors[record[0]][record[1]] = 1
        except KeyError:
            print record
            exit()

    return vectors, users, stats


def wrapper(p_singles, vectors, chunk):
    return [(m, calculate_p_single(m, 3883, 6040, vectors)) for m in chunk]


if __name__ == "__main__":
    main()
