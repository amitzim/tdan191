from random import sample, choice, randint
import cluster
from utils import *
from copy import deepcopy

def swapping(Cluster):
    clust1, clust2 = sample(Cluster, 2)
    index1 = Cluster.index(clust1)
    index2 = Cluster.index(clust2)
    cand1 = choice(clust1.movies)
    cand2 = choice(clust2.movies)
    Cluster[index1].remove(cand1)
    Cluster[index2].remove(cand2)
    Cluster[index1].add_movie(cand2)
    Cluster[index2].add_movie(cand1)
    return Cluster


def split_cluster(Cluster):
    cluster_candidate = choice(Cluster)
    while len(cluster_candidate) == 1:
        cluster_candidate = choice(Cluster)
        if reduce(lambda x, y: x and len(y) == 1, Cluster, True):
            return Cluster
    index = Cluster.index(cluster_candidate)
    cand = choice(cluster_candidate.movies)
    # print cluster_candidate, index, cand
    Cluster[index].remove(cand)
    new_clust = cluster.Cluster(cand)
    Cluster.append(new_clust)
    return Cluster


def move_movie_between_clusters(Cluster):
    clust1, clust2 = sample(Cluster, 2)
    index1 = Cluster.index(clust1)
    index2 = Cluster.index(clust2)
    try:
        cand = choice(clust1.movies)
    except IndexError:
        print "***Index error in move_movie_between_clusters()********"
        print clust1
        print index1
        print Cluster
        exit()
    Cluster[index2].add_movie(cand)
    if len(clust1) == 1:
        Cluster.remove(clust1)
    else:
        Cluster[index1].remove(cand)

    return Cluster


def unify_clusters(Cluster):
    # embed()
    try:
        clust1, clust2 = sample(Cluster, 2)
    except ValueError:
        # print "ERROR!\n\n\n\n\n\n"
        # print Cluster
        # exit(0)
        return Cluster
    Cluster.remove(clust1)
    Cluster.remove(clust2)
    new_clust = cluster.Cluster(create_dummy_movie())
    new_clust.from_movies(clust1.movies + clust2.movies)
    Cluster.append(new_clust)
    # embed()
    # exit(1)
    return Cluster


def change_Cluster(Cluster):
    changes = {1: swapping, 2: move_movie_between_clusters, 3: unify_clusters, 4: split_cluster}
    if len(Cluster) == 1:
        return split_cluster(Cluster)
    else:
        # changes = {1: swapping, 2: move_movie_between_clusters, 3: split_cluster}
        chosen_change_in_Cluster = choice(changes.keys())

    return changes[chosen_change_in_Cluster](Cluster)


def hill_climbing(movie_list, movies, p_singles, p_doubles, **kwargs):
    Cluster = make_random_cluster(movies)
    cost = calc_cluster_cost(Cluster, p_singles, p_doubles)
    best_config = Cluster
    best_cost = cost
    for i in xrange(1, 10000):
        current_config = change_Cluster(Cluster)
        current_cost = calc_cluster_cost(current_config, p_singles, p_doubles)
        # print "Current Cost:", current_cost, "|", "Best Cost:", best_cost, "|", "iteration:", i
        if current_cost < best_cost:
            best_config = current_config
            best_cost = current_cost

    return best_config


def make_random_cluster(movies_object):
    Cluster = []
    movies = deepcopy(movies_object)
    while len(movies) > 0:
        clust_size = randint(1, min(10, len(movies)))
        new_clust = cluster.Cluster(create_dummy_movie())
        new_clust.from_movies(sample(movies, clust_size))
        Cluster.append(new_clust)
        movies = list(set(movies) - set(new_clust.movies))
        # movies = [e for e in movies if e not in new_clust]

    # embed()

    return Cluster

