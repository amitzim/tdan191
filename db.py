import sqlite3

database_file_name = './database.db'


def fetch_ratings():
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("SELECT user_id, movie_id FROM Ratings")
        return cursor.fetchall()


def fetch_users_id():
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("SELECT user_id FROM Users")
        data = cursor.fetchall()
        users_list = list()
        # for rec in data:
        #     users_list.append(rec[0])

        # return users_list
        return [rec[0] for rec in data]


def fetch_movies_id():
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("SELECT movie_id FROM Movies")
        data = cursor.fetchall()
        movies_list = list()
        # for rec in data:
        #     movies_list.append(rec[0])

        # return movies_list
        return [rec[0] for rec in data]


def add_user_statistics():
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("ALTER TABLE Users ADD COLUMN movies_watched INTEGER")
        cursor.execute("""UPDATE Users
                                    SET movies_watched = (
                                        SELECT COUNT (user_id)
                                        FROM Ratings
                                        WHERE Ratings.user_id = Users.user_id
                                        GROUP BY user_id
                                        )""")
        dbcon.commit()


def get_all_users_statistics():
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("""SELECT user_id, movies_watched FROM Users""")

        return cursor.fetchall()


def get_user_movie_watched(user_id):
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("""SELECT movies_watched FROM Users WHERE Users.user_id == ?""", (user_id,))

        return cursor.fetchone()[0]


def add_p_couples(lst):
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        # cursor.execute("""CREATE TABLE Probs (
        #                     j INTEGER NOT NULL,
        #                     t INTEGER NOT NULL,
        #                     value REAL NOT NULL,
        #                     plus_or_minus TEXT)""")
        for rec in lst:
            cursor.execute("""INSERT INTO Probs (j, t, value) VALUES (?, ?, ?)""", rec)
        dbcon.commit()


def fetch_p_singles(movies):
    res = []
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        for m in movies:
            res.append((m, cursor.execute("SELECT p_single FROM Movies WHERE Movies.movie_id == ?", (m,)).fetchone()[0]))
    # print res
    return res


if __name__ == '__main__':
    pass
    # create_db()
    # load_users()
    # load_movies()
    # load_ratings()
