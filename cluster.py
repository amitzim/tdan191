import sys

class Cluster:
    def __init__(self, movie):
        self.movies = [movie]
        self.genre = movie.genre

    def from_movies(self, movies):
        self.movies = movies
        self.genre = self.movies[0].genre

    def add_movie(self, movie):
        if movie not in self.movies:
            # if self.genre_fits_to_cluster(self, movie):
            self.movies.append(movie)

    def genre_fits_to_cluster(self, movie):
        for g in self.genre:
            if g in movie.genre:
                return True
        return False

    def print_cluster(self):
        for m in self.movies:
            sys.stdout.write(str(m.id) + ' ' + str(m.title) + ', ')

    def merge(self, clust):
        self.movies += clust.movies
        return self

    def remove(self, movie):
        self.movies.remove(movie)

    def __str__(self):
        # return reduce(lambda x, y: x + "{0} {1},".format(y.id, y.title), self.movies, "")
        res = "{0} {1}".format(self.movies[0].id, self.movies[0].title)
        for movie in self.movies[1:]:
            res += ", {0} {1}".format(movie.id, movie.title)
        return res + '\n'

    def __len__(self):
        return len(self.movies)
