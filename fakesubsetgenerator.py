import random as rand
import os
import sqlite3
import sys

filename = './subset_movies.txt'
# database_file_name = 'database.db'
# database_file_name = './fake_database.db'
database_file_name = 'database.db'
SUBSET_SIZE = int(sys.argv[1])


def main():
    movie_ids_list = list()
    with sqlite3.connect(database_file_name) as dbcon:
        cursor = dbcon.cursor()
        movie_ids_list = cursor.execute("""SELECT movie_id
                                            FROM Movies
                                            WHERE movie_id IN
                                            (
                                                SELECT movie_id from Ratings
                                                GROUP BY movie_id
                                                HAVING COUNT(movie_id) >= 10
                                            )""").fetchall()
    try:
        os.remove(filename)
    except OSError:
        pass
    lst = []
    i = 0
    movie_ids_list = map(lambda x: x[0], movie_ids_list)

    with open(filename, 'w') as f:
        while i < SUBSET_SIZE:
            try:
                movie_id = rand.choice(movie_ids_list)
            except OSError:
                print "OSError"
                if movie_ids_list is []:
                    print "trying to select movie from empty set!"
                    exit()
            if movie_id not in lst:
                lst.append(movie_id)
                f.write(str(movie_id) + '\n')
                i += 1


if __name__ == "__main__":
    main()
