import copy
import itertools
import random as rand
import time
from functools import partial
from multiprocessing.pool import ThreadPool
import cluster
from utils import calc_cluster_cost


def our_algorithm_triples(movie_list, movies_objects, p_singles, p_doubles, **kwargs):
    vectors, stats = kwargs["vectors"], kwargs["stats"]
    p_triples = calc_p_triples(vectors, movie_list, stats)
    # print "Finished calculate triples after: ", time.time() - starttime

    Cluster = []
    remaining_movies = copy.deepcopy(movies_objects)

    # print "finished deepcopy:", time.time() - starttime
    while remaining_movies:
        pivot = rand.choice(remaining_movies)
        clust = cluster.Cluster(pivot)
        remaining_movies.remove(pivot)
        for movie2 in remaining_movies:
            for movie3 in remaining_movies:
                if movie2 == movie3:
                    continue
                try:
                    if p_singles[pivot.id] * p_singles[movie2.id] * p_singles[movie3.id] <= p_triples[tuple(sorted([pivot.id, movie2.id, movie3.id]))]:
                        clust.add_movie(movie3)
                        remaining_movies.remove(movie3)
                        continue
                except KeyError:
                    t = tuple(sorted([movie2, movie3, pivot]))
                    print "is %s in p_triples?" % t.__str__(), t in p_triples
                    exit()
            clust.add_movie(movie2)
            remaining_movies.remove(movie2)
        Cluster.append(clust)

    return Cluster


def calc_p_triples(vectors, movies, stats):
    workers = 4
    executor = ThreadPool(workers)
    my_wrapper = partial(calc_chunk_triple, vectors, stats)

    subsets = list(itertools.combinations(movies, 3))
    res = []
    p_triples = []
    chunk_size = len(subsets) / workers
    chunks = [subsets[x:x + chunk_size] for x in xrange(0, len(subsets), chunk_size)]
    res += executor.map(my_wrapper, chunks)
    for rec in res:
        p_triples += rec
    executor.close()
    return dict(p_triples)


def calculate_p_triple(m1, m2, m3, vectors, stats):
    N = 6040
    k = 3883
    one_over_n_plus_one = 1. / (N + 1)
    two_over_k = 2. / (k * (k - 1) * (k - 2))
    p_movie = 0.
    users_watched_all = [k for k, v in vectors.iteritems() if (v.get(m1) and v.get(m2) and v.get(m3))]

    # new implementation - BAD
    movies_watched_by_users = [stats[user_id] for user_id in users_watched_all]
    reduce(lambda x, n_i: x + (2. / (n_i * (n_i - 1) * (n_i - 2))), movies_watched_by_users, p_movie)

    # old implementation
    # for user_id in users_watched_all:
    #     n_i = stats[user_id]
    #     p_movie += (2. / (n_i * (n_i - 1) * (n_i - 2)))

    return one_over_n_plus_one * (two_over_k + p_movie)


def calc_chunk_triple(vectors, stats, chunk):
    res = []
    res = [((rec[0], rec[1], rec[2]), calculate_p_triple(rec[0], rec[1], rec[2], vectors, stats)) for rec in chunk]

    return res
