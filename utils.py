import itertools
import math
import sqlite3 as s
import time
from functools import partial
from multiprocessing.pool import ThreadPool
import db
import Movie


def create_movies(movie_list):
    with s.connect('./database.db') as dbcon:
        dbcon.text_factory = str
        cursor = dbcon.cursor()
        data = cursor.execute("""SELECT movie_id, title, genre FROM Movies WHERE Movies.movie_id IN %s group by movie_id""" % tuple(movie_list).__str__()).fetchall()

    return map(Movie.Movie, data)


# def my_print_cluster(Cluster):
#     # for c in Cluster:
#     #     for m in
#     x = map(lambda x: my_print_cluster(x) if isinstance(x, list) else x.id, Cluster)
#     return x


# def print_cluster_old(Cluster):
#     for c in Cluster:
#         for movie in c.movies:
#             sys.stdout.write(str(movie.id) + ' ' + str(movie.title) + ', ')
#         sys.stdout.write('\n')
#
#     sys.stdout.flush()


# def print_cluster_from_object_cluster(Cluster):
#     for c in Cluster:
#         c.print_cluster()
#         sys.stdout.write('\n')
#
#     sys.stdout.flush()


def cluster_to_string(Cluster):
    # type: (list) -> string
    res = ""
    for clust in Cluster:
        res += clust.__str__()

    return res


def calc_p_singles_and_doubles(vectors, users, movies, stats):
    # start = time.time()
    # chunks = [movies[x:x + 500] for x in xrange(0, len(movies), 500)]
    # executor = ThreadPool(10)
    # p_singles = dict()
    # from functools import partial
    # my_wrapper = partial(wrapper, p_singles, vectors)
    # print "before map"
    # res = []
    # res += executor.map(my_wrapper, chunks)
    # # res = dict(res)
    # # print res
    # embed()
    # exit
    # p_singles = dict([(m, calculate_p_single(m, 3883, 6040, vectors)) for m in movies])
    p_singles = dict(db.fetch_p_singles(movies))

    # print "time took to calc singles: ", time.time() - start
    start = time.time()
    workers = 4
    executor = ThreadPool(workers)
    # print "Workers: ", workers
    my_wrapper = partial(calc_chunk, vectors, stats)
    subsets = list(set(itertools.combinations(movies, 2)))
    res = []
    p_doubles = []
    chunk_size = len(subsets) / workers
    chunks = [subsets[x:x + chunk_size] for x in xrange(0, len(subsets), chunk_size)]

    res += executor.map(my_wrapper, chunks)
    for rec in res:
        p_doubles += rec
    executor.close()
    # print "time took to calc doubles: ", time.time() - start
    return p_singles, dict(p_doubles)


def set_tuple(pivot, movie):
    return (movie, pivot) if pivot > movie else (pivot, movie)


def calc_chunk(vectors, stats, chunk):
    res = []
    # start = time.time()
    # for rec in chunk:
    res = [(set_tuple(rec[0], rec[1]), calculate_p_couple(rec[0], rec[1], vectors, stats)) for rec in chunk]
    # print "Time took to calc Chunk", time.time() - start, ", chunk size:", len(chunk)

    return res
    # res.append((rec[0], rec[1], calculate_p_couple(rec[0], rec[1], vectors, stats)))


def calculate_p_couple(m1, m2, vectors, stats):
    # global k
    # global N
    # global vectors
    # start = time.time()

    # v1
    # N = 6040
    # k = 3883
    # one_over_n_plus_one = 1. / (N + 1)
    # two_over_k = 2. / (k * (k - 1))

    # v2: 200ms faster than v1 per chunk. maybe less accurate
    one_over_n_plus_one = 0.00016553550736633007 # 1. / (N + 1)
    two_over_k = 1.32680492239319e-07 # 2. / (k * (k - 1))


    p_movie = 0.
    # print "calculate_p_couple: after declaring vars: ", time.time() - start

    ################## a bit slower than "new implementation"
    # users_watched_both = [k for k, v in vectors.iteritems() if (v.get(m1) and v.get(m2))]
    # relevant_stats = map(lambda x: stats[x], users_watched_both)
    # p_movie = reduce(lambda x, y: x + (2. / (y * (y - 1))), relevant_stats, 0.)
    ##################


    # new implementation
    users_watched_both = [k for k, v in vectors.iteritems() if (v.get(m1) and v.get(m2))]

    # movies_watched_by_users = [stats[user_id] for user_id in users_watched_both]
    # reduce(lambda x, n_i: x + (2. / (n_i * (n_i - 1))), movies_watched_by_users, p_movie)

    for user_id in users_watched_both:
        n_i = stats[user_id]
        p_movie += (2. / (n_i * (n_i - 1)))
    # end of new implementation

    # start of old implementation
    # for user_id, movie_vector in vectors.iteritems():
    #     v_j = movie_vector.get(m1)
    #     v_t = movie_vector.get(m2)
    #     if not (v_j and v_t):
    #         continue
    #     # print "user_id: ", user_id
    #     n_i = stats[user_id]
    #     # print "n_i:", n_i
    #     # exit()
    #     # embed()
    #     # n_i = sum(movie_vector.values())
    #
    #     p_movie += (2. / (n_i * (n_i - 1)))
    # End of old implementation

    return one_over_n_plus_one * (two_over_k + p_movie)


def calc_cluster_cost(Cluster, p_singles, p_doubles):
    res = 0
    for c in Cluster:
        res += calc_cluster(c.movies, p_singles, p_doubles)

    return res


def calc_cluster(c, p_singles, p_doubles):
    cluster_size = len(c)
    if cluster_size == 1:
        return math.log(1./p_singles[c[0].id])
    else:
        subsets = list(itertools.combinations(c, 2))
        one_over_c_minus_one = 1. / (cluster_size - 1)

        return reduce(lambda x, y: x + one_over_c_minus_one * math.log(1. / p_doubles.get(set_tuple(y[0].id, y[1].id))),
                      subsets, 0)
        # return reduce(f, subsets, 0)


def create_dummy_movie():
    return Movie.Movie((-1, "null", "null"))
